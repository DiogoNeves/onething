//
//  OneThing+CoreDataProperties.swift
//  OneThing
//
//  Created by Diogo Neves on 21/02/2016.
//  Copyright © 2016 Diogo Neves. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension OneThing {

    @NSManaged var currentFocus: String?
    @NSManaged var appOpensCount: Int

}
