//
//  ViewController.swift
//  OneThing
//
//  Created by Diogo Neves on 21/02/2016.
//  Copyright © 2016 Diogo Neves. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: - UI
    
    private let imageAssets = ["pablo-3", "pablo-4"]
    private var appOpensCount: Int = 0
    
    @IBOutlet weak var currentImageView: UIImageView!
    @IBOutlet weak var currentFocusField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.hidesNavigationBarHairline = true
        self.incrementAppOpens()
        self.updateUI()
    }
    
    private func updateUI() {
        self.updateImage()
        self.updateCurrentFocusUI()
    }
    
    private func updateImage() {
        let imageIndex = self.appOpensCount % self.imageAssets.count
        self.currentImageView.image = UIImage(named: self.imageAssets[imageIndex])
    }
    
    private func updateCurrentFocusUI() {
        self.currentFocusField.delegate = self
        self.currentFocusField.text = self.fetchCurrentFocus()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Save/Load Data
    
    private func incrementAppOpens() {
        let dataController = DataController().managedObjectContext
        var entity: OneThing! = self.fetchCurrentFocusEntity(dataController)
        
        if entity == nil {
            entity = NSEntityDescription.insertNewObjectForEntityForName("OneThing", inManagedObjectContext: dataController) as! OneThing
        }
        
        let previousAppOpensCount = entity.appOpensCount
        self.appOpensCount = previousAppOpensCount + 1
        entity.setValue(self.appOpensCount, forKey: "appOpensCount")
        
        do {
            print("Incrementing app opens")
            try dataController.save()
        } catch {
            fatalError("Failed to save entity: \(error)")
        }
    }
    
    private func saveCurrentFocus(focus: String?) {
        let dataController = DataController().managedObjectContext
        var entity: OneThing! = self.fetchCurrentFocusEntity(dataController)
        
        if entity == nil {
            entity = NSEntityDescription.insertNewObjectForEntityForName("OneThing", inManagedObjectContext: dataController) as! OneThing
        }
        
        entity.setValue(focus, forKey: "currentFocus")
        
        do {
            print("Saving current focus")
            try dataController.save()
        } catch {
            fatalError("Failed to save entity: \(error)")
        }
    }
    
    private func fetchCurrentFocus() -> String? {
        let dataController = DataController().managedObjectContext
        return self.fetchCurrentFocusEntity(dataController)?.currentFocus
    }
    
    private func fetchCurrentFocusEntity(dataController: NSManagedObjectContext) -> OneThing? {
        let focusFetch = NSFetchRequest(entityName: "OneThing")
        
        do {
            let fetchedFocusList = try dataController.executeFetchRequest(focusFetch) as! [OneThing]
            if let fetchedFocus = fetchedFocusList.first {
                print("Loaded focus \(fetchedFocus.currentFocus)")
                return fetchedFocus
            } else {
                print("No saved focus yet")
            }
            
        } catch {
            fatalError("Failed to fetch person: \(error)")
        }
        
        return nil
    }
    
    // MARK: - Logic

    @IBAction func didChangeFocus(sender: UITextField) {
        self.saveCurrentFocus(sender.text)
    }

    @IBAction func didPressComplete(sender: UIButton) {
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}

